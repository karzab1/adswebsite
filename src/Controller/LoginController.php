<?php

namespace App\Controller;

use App\Entity\Login;
use App\Form\LoginType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\Mapping as ORM;

class LoginController extends AbstractController

{
    /**
     * @Route("/login", name="login")
     * Method({"GET", "POST"})
     */
    public function create(Request $request): Response
    {
        $login =  new login();
        $login->setCreationDate(new \DateTime());
        $form = $this->createForm(LoginType::class, $login, [
            'action' => $this->generateUrl("login")
        ]);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $advertisement = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($advertisement);
            $entityManager->flush();
            return $this->redirectToRoute("home");
        }


        return $this->render('login/login.html.twig', [
            'form' => $form->createView(),
        ]);

    }

}