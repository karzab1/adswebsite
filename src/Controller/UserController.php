<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\User;
use App\Entity\Advertisement;
use App\Form\AdvertisementType;
use App\Controller\AdvertisementController;
use App\Form\RegisterType;
use App\Form\UserType;
use ContainerBFyPlmA\getDataCollector_Request_SessionCollectorService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class UserController extends AbstractController
{
    /**
     * @Route ("/user/{email}", name= "user_info")
     * @param $email
     */
    public function show($email)
    {
        $user = $this ->getDoctrine()->getRepository(User::class) ->find($email);

        return $this ->render('user/show.html.twig',array('user'=>$user));
    }

    /**
     * @Route ("/user/edit/{id}", name="user_info_edit")
     * Method({"GET", "POST"})
     */
    public function edit (Request $request,$id):Response
    {
        $user = $this ->getDoctrine()->getRepository(User::class)->find($id);

        $form = $this -> createForm(UserType::class,$user);

        $form -> handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $user = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();

            $advertisements= $this->getDoctrine() ->
            getRepository(Advertisement::class)->findAll();

            return $this ->render('user/show.html.twig', [
            'user' => $user,
            'advertisements' => $advertisements]);
        }

        return $this -> render('user/edit.html.twig', [
            'form' => $form ->createView(),]);


    }
}
