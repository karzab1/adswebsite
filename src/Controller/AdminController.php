<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
//use Symfony\Component\BrowserKit\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class AdminController extends AbstractController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {

        $categories = $this->getDoctrine()->getRepository(Category::class)->findBy(["parent" => null]);

        return $this->render('admin/index.html.twig', [

            "categories" => $categories
        ]);
    }

    /**
     * @Route("/admin/category/create/{id}", name="category_create", defaults={"id"=0})
     * Method({"GET", "POST"})
     */
    public function create(Request $request, int $id): Response
    {
        $category = new Category();

        $form = $this->createForm(CategoryType::class, $category, [
            'action' => $this->generateUrl("category_create", ["id" => $id])
        ]);

        $form->handleRequest($request);


        if($form->isSubmitted() && $form->isValid()) {
            $category = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $category->setParent(null);

            if($id > 0)
            {
                $parent = $this->getDoctrine()->getRepository(Category::class)->find($id);

                $category->setParent($parent);
                $parent->addCategory($category);
            }


            $entityManager->persist($category);
            $entityManager->flush();

            return $this->redirectToRoute("admin");
        }

        return $this->render('admin/create.html.twig', [
            'form' => $form->createView(),]);

    }


    /**
     * @Route("/admin/category/edit/{id}", name="category_edit")
     * Method({"GET", "POST"})
     */
    public function edit(Request $request, $id): Response
    {
        $category = $this->getDoctrine()->getRepository(Category::class)->find($id);

        $form = $this->createForm(CategoryType::class, $category);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $category = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();

            return $this->redirectToRoute("admin");
        }

        return $this->render('admin/edit.html.twig', [
            'form' => $form->createView(),]);
    }

    /**
     * @Route("/admin/category/delete/{id}")
     * Method({"DELETE", "GET"})
     */
    public function delete(Request $request, $id): Response
    {
        $category = $this->getDoctrine()->getRepository(Category::class)->find($id);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($category);
        $entityManager->flush();

        return $this->redirectToRoute("admin");
    }
}
