<?php

namespace App\Controller;

use App\Entity\Advertisement;
use App\Entity\Category;
use App\Entity\User;
use App\Form\AdvertisementType;
use ContainerBFyPlmA\getDataCollector_Request_SessionCollectorService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\String\Slugger\SluggerInterface;

class AdvertisementController extends AbstractController
{
    /**
     * @Route("/", name="advertisement_list")
     * @Method ({"GET"})
     */
    public function index()
{
    $advertisements= $this->getDoctrine() ->
    getRepository(Advertisement::class)->findAll();

    $categories = $this->getDoctrine()->getRepository(Category::class)->findBy(["parent" => null]);

    return $this->render('home/index.html.twig', [
        'advertisements'=>$advertisements,
        'categories'=>$categories
    ]);
}

    /**
     * @Route("/advertisement/create", name="advertisement_create")
     * Method({"GET", "POST"})
     */
    public function create(Request $request, SluggerInterface $slugger): Response
    {
        $advertisement =  new Advertisement();

        $advertisement->setCreationDate(new \DateTime());

        $form = $this->createForm(AdvertisementType::class, $advertisement, [
            'action' => $this->generateUrl("advertisement_create")
        ]);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $imageFile = $form->get('imageFile')->getData();
            $advertisement = $form->getData();
            $user = new User();
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $advertisement->setEmail($user->getEmail());

            if($imageFile)
            {
                $ogImageFilename = pathinfo($imageFile -> getClientOriginalName(),PATHINFO_FILENAME);
                $safeImageFilename = $slugger -> slug($ogImageFilename);
                $newImageFilename = $safeImageFilename.'-'.uniqid().'.'.$imageFile->guessExtension();

                try{
                    $imageFile -> move(
                        $this ->getParameter('images_directory'),
                        $newImageFilename
                    );


                } catch (FileException $e) {

                }

                $advertisement -> setImageFile($newImageFilename);
            }


            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($advertisement);
            $entityManager->flush();

            return $this->redirectToRoute("home");
        }

        return $this->render('advertisement/create.html.twig', [
            'form' => $form->createView(),
            ]);
    }

    /**
     * @Route("/advertisement/edit/{id}", name="advertisement_edit")
     * Method({"GET", "POST"})
     */
    public function edit(Request $request, $id, sluggerInterface $slugger): Response
    {
        $advertisement = $this->getDoctrine()->getRepository(Advertisement::class)
            ->find($id);

        $form = $this->createForm(AdvertisementType::class, $advertisement);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $advertisement = $form ->getData();
            $oldImage = $advertisement -> getImageFile();
            $imageFile = $form->get('imageFile')->getData();

            if(!empty($imageFile))
            {
                $ogImageFilename = pathinfo($imageFile -> getClientOriginalName(),PATHINFO_FILENAME);
                $safeImageFilename = $slugger -> slug($ogImageFilename);
                $newImageFilename = $safeImageFilename.'-'.uniqid().'.'.$imageFile->guessExtension();

                try{
                    $imageFile -> move(
                        $this ->getParameter('images_directory'),
                        $newImageFilename
                    );

                } catch (FileException $e) {

                }

                $advertisement -> setImageFile($newImageFilename);
            }
            else{
                if(!empty($oldImage)){
                    $advertisement -> setImageFile($oldImage);
                }
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();

            return $this ->render('advertisement/show.html.twig', array
            ('advertisement' => $advertisement));
        }

        return $this->render('advertisement/edit.html.twig', [
            'form' => $form->createView(),]);
    }

    /**
     * @Route("/advertisement/delete/{id}")
     * Method({"DELETE", "GET"})
     */
    public function delete(Request $request, $id){
        $advertisement = $this->getDoctrine()->getRepository
        (Advertisement::class)->find($id);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($advertisement);
        $entityManager->flush();

        return $this->redirectToRoute("home");
    }


    /**
     * @Route("/advertisement/{id}", name="advertisement_show")
     * @param $id
     *
     * @return Response
     */
    public function show($id) {

        $advertisement = $this->getDoctrine()->getRepository(Advertisement::class)->find($id);

        return $this ->render('advertisement/show.html.twig', array
        ('advertisement' => $advertisement));

    }

    /**
     * @Route("/category/{id}", name="category_list")
     * @param $id
     *
     * @return Response
     */
    public function showCategory($id): Response
    {
        $categoriesDisplay = $this->getDoctrine()->getRepository(Category::class)->findBy(["parent" => null]);
        $category = $this->getDoctrine()->getRepository(Category::class)->find($id);

        if(!$category)
        {
            return $this->redirectToRoute("home");
        }

        $advertisements = null;
        if($category->getParent() === null)
        {
            $subcategories = [];
            foreach($category->getCategories() as $subcategory)
            {
                array_push($subcategories, $subcategory->getId());
            }

            $advertisements = $this->getDoctrine()->getRepository(Advertisement::class)->findBy(['category' => $subcategories]);
        }
        else
        {
            $advertisements = $this->getDoctrine()->getRepository(Advertisement::class)->findBy(['category' => $id]);
        }


        //$advertisements = $this->getDoctrine()->getRepository(Advertisement::class)->findBy(["category" => $id]);


        return $this->render('home/index.html.twig', [
            'advertisements'=>$advertisements,
            //'category'=>$category,
            'categories' => $categoriesDisplay
        ]);
    }

    /**
     * @Route("/user/{email}", name="user_advertisement_list")
     * @Method ({"GET"})
     */
    public function listAtUser()
    {
        $advertisements= $this->getDoctrine() ->
        getRepository(Advertisement::class)->findAll();

        $categories = $this->getDoctrine()->getRepository(Category::class)->findBy(["parent" => null]);

        return $this->render('user/show.html.twig', [
            'advertisements'=>$advertisements,
            'categories'=>$categories
        ]);
    }
}
