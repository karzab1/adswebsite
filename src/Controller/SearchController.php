<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\RegisterType;
use App\Form\UserType;
use App\Repository\AdvertisementRepository;
use ContainerBFyPlmA\getDataCollector_Request_SessionCollectorService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class SearchController extends AbstractController
{
    /**
     * @Route("/search", name="search")
     */

    public function index()
    {
        return $this -> render('search/index',[
            'controller_name' => 'SearchController',
    ]);
    }

    public function searchBar()
    {
        $form = $this ->createFormBuilder(null)
            ->setAction($this->generateUrl('handleSearch'))
            ->add('query',TextType::class,[
                'label'=>false,
            ])
            ->add('search', SubmitType::class,[
                'attr'=> [
                    'class' => 'btn btn-secondary'
                ]
            ])
            ->getForm();

        return $this->render('home/searchBar.html.twig',[
            'form'=>$form->createView()
        ]);
    }

    /**
     * @Route("/handleSearch", name="handleSearch")
     * @param Request $request
     */
    public function handleSearch(Request $request, AdvertisementRepository $advertisementRepository)
    {
        $query = $request->request->get('form')['query'];

        if($query)
        {
            $advertisements = $advertisementRepository ->findAdsByTitle($query);
            $categories = $this->getDoctrine()->getRepository(Category::class)->findBy(["parent" => null]);

        }

        return $this->render('home/index.html.twig',[
            'advertisements'=>$advertisements,
            'categories'=>$categories
        ]);
    }
}
