<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Category;

class CategoryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        $category = new Category();

        $category->setTitle('Transportas')->setParent(null);
        $manager->persist($category);

        $category2 = new Category();
        $category2->setTitle('Automobilis')->setParent($category);
        $manager->persist($category2);
        $category->addCategory($category2);

        $category3 = new Category();
        $category3->setTitle('Nekilnojamas turtas')->setParent(null);
        $manager->persist($category3);

        $category4 = new Category();
        $category4->setTitle('Motociklas')->setParent($category);
        $manager->persist($category4);
        $category->addCategory($category4);

        $category5 = new Category();
        $category5->setTitle('Dviratis')->setParent($category);
        $manager->persist($category5);
        $category->addCategory($category5);

        $category6 = new Category();
        $category6->setTitle('Namas')->setParent($category3);
        $manager->persist($category6);
        $category3->addCategory($category6);

        $manager->flush();
    }
}
