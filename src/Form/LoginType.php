<?php


namespace App\Form;


use App\Entity\Login;

use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\Extension\Core\Type\FileType;

use Symfony\Component\Form\Extension\Core\Type\NumberType;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Symfony\Component\Form\Extension\Core\Type\TextareaType;

use Symfony\Component\Form\Extension\Core\Type\TextType;

use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Validator\Constraints\NotBlank;

use Symfony\Component\Validator\Constraints\Regex;

use Symfony\Component\Validator\Constraints\Length;


class LoginType extends AbstractType

{

    public function buildForm(FormBuilderInterface $builder, array $options)

    {

        $builder

            ->add('name', TextType::class, [

                'attr' => [

                    'class' => 'form-control'

                ],

                'required' => true,

                'constraints' => [

                    new Length( ['min' => 4] )

                ]

            ])
            ->add('password', TextType::class, [

                'attr' => [

                    'class' => 'form-control'

                ],

                'required' => true,

                'constraints' => [

                    new Length( ['min' => 6] )

                ]

            ])
            ->add('email', TextType::class, [

                'attr' => [

                    'class' => 'form-control'

                ],

                'required' => true,

                'constraints' => [

                    new Length( ['min' => 4] )

                ]

            ])

            ->add('save', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-warning'
                ]
            ])
        ;
    }


    public function configureOptions(OptionsResolver $resolver)

    {

        $resolver->setDefaults([

            'data_class' => Login::class,

        ]);

    }

}