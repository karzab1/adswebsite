<?php


namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => "El. paštas",
                'attr' => [
                    'class' => 'form-control'
                ],
                'required' => true
            ])
            ->add('first_name', TextType::class, [
                'label' => "Vardas",
                'attr' => [
                    'class' => 'form-control'
                ],
                'required' => True
            ])
            ->add('last_name', TextType::class, [
                'label' => "Pavardė",
                'attr' => [
                    'class' => 'form-control'
                ],
                'required' => true
            ])
            ->add('phone', TextType::class, [
                'label' => "Tel. nr",
                'attr' => [
                    'class' => 'form-control'
                ],
                'required' => false
            ])
            ->add('city', TextType::class, [
                'label' => "Miestas",
                'attr' => [
                    'class' => 'form-control'
                ],
                'required' => false
            ])
            ->add('save', SubmitType::class, [
                'label' => "Išsaugoti",
                'attr' => [
                    'class' => 'btn btn-secondary'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
        ));
    }
}