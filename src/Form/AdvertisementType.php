<?php

namespace App\Form;

use App\Entity\Advertisement;
use App\Entity\Category;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\File;

class AdvertisementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('adTitle', TextType::class, [

                'label' => "Pavadinimas",
                'attr' => [
                    'class' => 'form-control'
                ],
                'required' => true,
                'constraints' => [
                    new Length( ['min' => 4] )
                ]
            ])
            ->add('description', TextType::class, [
                'label' => "Aprašymas",
                'attr' => [
                    'class' => 'form-control'
                ],
                'required' => true,
                'constraints' => [
                    new Length( ['min' => 4] )
                ]
            ])
            ->add('price', NumberType::class, [
                'label' => "Kaina",
                'attr' => [
                    'class' => 'form-control',
                ],
                'required' => true,
            ])

            ->add('category', EntityType::class, [
                'attr' => [
                    'class' => 'form-control',
                ],
                'placeholder' => 'Pasirinkite kategoriją',
                'class' => Category::class,
                'choice_label' => 'title',
                'required' => true,

            ])

            ->add('conditionStatus', TextType::class, [
                'label' => "Būklė",
                'attr' => [
                    'class' => 'form-control'
                ],
                'required' => true,
                'constraints' => [
                    new Length( ['min' => 4] )
                ]
            ])
            ->add('phoneNumber', TextType::class, [

                'label' => "Telefono nr.",
                'attr' => [
                    'class' => 'form-control'
                ],
                'required' => true,
                'constraints' => [
                    new NotBlank(),
                    new Regex('/\+370[0-9]{8}/'),
                    //new Length( ['min' => 12, 'max' => 12] )
                ]
            ])
            ->add('city', TextType::class, [
                'label' => 'Miestas',
                'attr' => [
                    'class' => 'form-control',
                ],
                'required' => true,
                'constraints' => [
                    new Length( ['min' => 4] )
                ]
            ])
            ->add('website', TextType::class, [
                'label' => "Svetainė",
                'attr' => [
                    'class' => 'form-control'
                ],
                'required' => false
            ])

            ->add('imageFile', FileType::class, [
                'label' => "Nuotrauka",
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'class' => 'btn'
                ],
                'constraints' => [
                    new File([
                        'maxSize' => '1240k',
                        /*'mimeTypes' => [
                            'application/jpg',
                            'application/png',
                            'application/jpeg'
                        ],*/
                        'mimeTypesMessage' => 'Netinkamas nuotraukos formatas'
                    ])
                ]
            ])

            ->add('save', SubmitType::class, [
                'label' => "Išsaugoti",
                'attr' => [
                    'class' => 'btn btn-secondary'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Advertisement::class,
        ]);
    }
}
