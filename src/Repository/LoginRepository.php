<?php


namespace App\Repository;

use App\Entity\Login;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

use Doctrine\Persistence\ManagerRegistry;


/**

 * @method Login|null find($name, $lockMode = null, $lockVersion = null)

 * @method Login|null findOneBy(array $criteria, array $orderBy = null)

 * @method Login[]    findAll()

 * @method Login[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)

 */

class LoginRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Login::class);
    }


    // /**

    //  * @return Advertisement[] Returns an array of Advertisement objects

    //  */

    /*

    public function findByExampleField($value)

    {

        return $this->createQueryBuilder('a')

            ->andWhere('a.exampleField = :val')

            ->setParameter('val', $value)

            ->orderBy('a.id', 'ASC')

            ->setMaxResults(10)

            ->getQuery()

            ->getResult()

        ;

    }

    */


    /*

    public function findOneBySomeField($value): ?Advertisement

    {

        return $this->createQueryBuilder('a')

            ->andWhere('a.exampleField = :val')

            ->setParameter('val', $value)

            ->getQuery()

            ->getOneOrNullResult()

        ;

    }

    */

}