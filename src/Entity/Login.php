<?php


namespace App\Entity;


use App\Repository\LoginRepository;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass=LoginRepository::class)
 */

class Login
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */

    private $name;


    /**
     * @ORM\Column(type="text")
     */

    private $password;


    /**
     * @ORM\Column(type="text")
     */

    private $email;

    /**
     * @ORM\Column(type="datetime")
     */

    private $creationDate;



    public function getName(): ?string
    {
        return $this->name;
    }
    public function getPassword(): ?string
    {
        return $this->password;
    }
    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->creationDate;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }
    public function setPassword(string $password): self
    {
        $this->password = $password;
        return $this;
    }
    public function setEmail(string $email): self
    {
        $this->email = $email;
        return $this;
    }

    public function setCreationDate(\DateTimeInterface $date): self
    {
        $this->creationDate = $date;
        return $this;
    }
}